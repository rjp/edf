package edf

import (
_    "fmt"
    "errors"
)

type Spangle *[]*EDF_Tree

type ParserData struct {
    EDF_root []*EDF_Tree
    qspangles Spangle
    parents []Spangle
    tokens []int
    depth int
}

var globalData ParserData
    
// Append a new leaf node `flanges=badger` to our current tree
func CreateLeaf(flanges []byte, badger []byte) {
    tmpNode := New_node(flanges, badger)
    *globalData.qspangles = append(*globalData.qspangles, &tmpNode)
}

// Add a new non-leaf node
func CreateNode(flanges []byte, badger []byte) {
    tmpNode := New_node(flanges, badger)

    // If we're at the top, create a new tree
    if globalData.depth == 0 {
        globalData.EDF_root = append(globalData.EDF_root, &tmpNode)
    }

    globalData.parents = append(globalData.parents, globalData.qspangles)

    globalData.depth = globalData.depth + 1

    if globalData.qspangles != nil {
        *globalData.qspangles = append(*globalData.qspangles, &tmpNode)
    }

    // Remember where our new node keeps its children.
    globalData.qspangles = &tmpNode.Children
}

// Pop a tree off our stack of trees
func pop_tree(a []Spangle) (Spangle, []Spangle) {
    return a[len(a)-1], a[:len(a)-1]
    }

// Pop an integer off our stack of integers
func pop(a []int) (int, []int) {
    return a[len(a)-1], a[:len(a)-1]
    }

// Pop a string off our stack of strings
func pop_s(a []string) (string, []string) {
    return a[len(a)-1], a[:len(a)-1]
    }

    %%{
  machine simple_lexer;
  write data;
  }%%

// Bog standard Ragel wrapper
func Parse(data []byte) ([]*EDF_Tree, error, int) {
    cs, p, pe, eof := 0, 0, len(data), len(data)
    s := -1
    badger := []byte{}
    flanges := []byte{}

    globalData = ParserData{}
    globalData.depth = 0

    tree_start := -2
    tree_span  := -1

    stack := [256]int{}
    top := 0

%%{


  action start_me_up {
//    fmt.Printf("ts=%d\n", p)
    tree_start = p
  }

  action end_tree {
	tree_span = p - tree_start

    // Remember how many bytes we parsed for the current tree
	globalData.EDF_root[len(globalData.EDF_root)-1].Bytes = tree_span

    tree_start = -1

  }

# We've seen a quote, mark the position
  action start_string {
    globalData.tokens = append(globalData.tokens, p)
  }
# We've seen an end-quote, stack the string
  action end_string {
    s, globalData.tokens = pop(globalData.tokens)
    badger = data[s:p]
  }

  action start_number {
      globalData.tokens = append(globalData.tokens, p)
  }
  action end_number {
      s, globalData.tokens = pop(globalData.tokens)
      badger = data[s:p]
  }
  action start_token {
      globalData.tokens = append(globalData.tokens, p)
  }
  action end_token {
      s, globalData.tokens = pop(globalData.tokens)
      s = s + 1
      flanges = data[s:p]
  }
  action stack_value {
      s, globalData.tokens = pop(globalData.tokens)
      value = data[s:p]
  }
  action add_leaf_noval {
    CreateLeaf(flanges, nil)
  }
  action add_leaf_string {
    CreateLeaf(flanges, badger)
  }
  action add_leaf_number {
    CreateLeaf(flanges, badger)
  }
  action add_node_noval {
    CreateNode(flanges, nil)
  }
  action add_node_string {
    CreateNode(flanges, badger)
  }
  action add_node_number {
    CreateNode(flanges, badger)
  }
  action close_node {
    globalData.qspangles, globalData.parents = pop_tree(globalData.parents)
    globalData.depth = globalData.depth - 1
    if globalData.depth == 0 {
    }
  }

  number = [0-9]+ >start_number %end_number;
  token = [a-zA-Z][_a-zA-Z]*;

  stringchar = [^"\\]+;
  escaped = /\\./;
  string = '"' (stringchar|escaped)* >start_string '"' @end_string;

    tree = '<' >start_token token %end_token ('' %add_node_noval | '=' number %add_node_number | '=' string %add_node_string) '>' @{ fcall tree_rest; } @close_node;
    leaf = '<' >start_token token %end_token ('' %add_leaf_noval | '=' number %add_leaf_number | '=' string %add_leaf_string) '/>';
    tree_rest := (leaf|tree)* '</>' @close_node @{ fret; };
    flange = '' >start_me_up tree;

    main := ( flange %end_tree )+;

  write init;
  write exec;
}%%

    if cs == simple_lexer_error {
        return globalData.EDF_root, errors.New("Bork"), tree_span
    }
    if cs == simple_lexer_first_final {
        return globalData.EDF_root, nil, tree_span
    }

    return globalData.EDF_root, errors.New("Unknown Bork"), tree_span
}

