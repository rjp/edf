package edf

import (
    "fmt"
	"strings"
"errors"
)

type EDF_Type int

const (
	EDF_String EDF_Type = 1
	EDF_Number EDF_Type = iota
	EDF_NoVal  EDF_Type = iota
)

type EDF_Tree struct {
	Key      string
	Value    interface{}
	Type     EDF_Type
	Children []*EDF_Tree
	Bytes    int
}

func as_tree_helper(t *EDF_Tree, depth int) {
    v := "<nil>"
    switch t.Value.(type) {
    case string:
        v = t.Value.(string)
    case int:
        v = string(t.Value.(int))
    case []uint8:
        v = string(t.Value.([]uint8))
    }
	fmt.Printf("%[1]*s%s = %s (%d)\n", depth, "", t.Key, v, len(t.Children))
	for _, q := range t.Children {
		as_tree_helper(q, depth+1)
	}
}

func As_tree(t EDF_Tree) {
	as_tree_helper(&t, 0)
}

func AddChild(t *EDF_Tree, c EDF_Tree) {
    t.Children = append(t.Children, &c)
}

func InterfaceToString(j interface{}) string {
	v := "<nil>"

	switch j.(type) {
	case string:
		v = j.(string)
	case int:
		v = string(j.(int))
	case []uint8:
		v = string(j.([]uint8))
	}

	return v
}

func (t EDF_Tree) ValueMap() (map[string]string) {
        bits := make(map[string]string)
        bits[t.Key] = InterfaceToString(t.Value)
        for _, j := range t.Children {
                bits[j.Key] = InterfaceToString(j.Value)
        }
        return bits
}

func (t EDF_Tree) TypeIs(s string) (bool) {
    return t.Key == s
}

func (t EDF_Tree) ValueOfTree(path string) (EDF_Tree, error) {
	parts := strings.Split(path, ".")
	bits := strings.Split(parts[0], "=")
	match := ""
	part := bits[0]
	if len(bits) > 1 {
		match = bits[1]
	}

	flange := append(t.Children, &t)
	for i := len(flange)-1; i >= 0; i-- {
		j := flange[i]
		found := false
		if j.Key == part {
			found = true
			v := InterfaceToString(j.Value)
			if match != "" {
				if v != match {
					found = false
				}
			}

		if found == false { continue }

			if len(parts) == 1 {
				return *j, nil
			} else {
				restOfPath := strings.Join(parts[1:], ".")
				return j.ValueOfTree(restOfPath)
			}
		}
	}
	return EDF_Tree{}, errors.New("[[not found]]")
}

func (t EDF_Tree) ValueOf(path string) (string, error) {
	parts := strings.Split(path, ".")
	bits := strings.Split(parts[0], "=")
	match := ""
	part := bits[0]
	if len(bits) > 1 {
		match = bits[1]
	}

	flange := append(t.Children, &t)
	for i := len(flange)-1; i >= 0; i-- {
		j := flange[i]
		found := false
		if j.Key == part {
			found = true
			v := InterfaceToString(j.Value)
			if match != "" {
				if v != match {
					found = false
				}
			}

		if found == false { continue }

			if len(parts) == 1 {
				v := InterfaceToString(j.Value)
				return v, nil
			} else {
				restOfPath := strings.Join(parts[1:], ".")
				return j.ValueOf(restOfPath)
			}
		}
	}
	return "[[not found]]", errors.New("[[not found]]")
}

func New_node(key interface{}, value interface{}) EDF_Tree {
	var t EDF_Type
	s := ""

	switch key.(type) {
	case string:
		s = key.(string)
	case []uint8:
		s = string(key.([]uint8))
	}

	switch value.(type) {
	case string:
		t = EDF_String
	case int:
		t = EDF_Number
    case nil:
        t = EDF_NoVal
	}
	x := EDF_Tree{s, value, t, []*EDF_Tree{}, -1}
	return x
}

