ragel = ragel
go = go
GOPATH := ${PWD}
export GOPATH

.PHONY: all build install

all: build

build: ragel-edf.go
	$(go) build
install:
	$(go) install
ragel-edf.go: ragel-edf.rl

# %.go: %.rl	; $(ragel) -Z -G2 -o $@ $<
%.go: %.rl	; $(ragel) -Z -e -o $@ $<
